package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.entities.TransactionsEntity;
import com.desafio2.bancomarcela.models.repositories.TransactionsRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/transactions")

public class TransactionsControllers {

    private final TransactionsRepository transactionsRepository;

    public TransactionsControllers(TransactionsRepository transactionsRepository){
        this.transactionsRepository=transactionsRepository;

        }

    @GetMapping("/{id}")
    public List<TransactionsEntity> getUserTransactions(@PathVariable Long id){
        return transactionsRepository.findAllById(id);

    }
    @PostMapping("/new")
    public TransactionsEntity newTransaction(@RequestBody TransactionsEntity transactionsEntity){
        return transactionsRepository.save(transactionsEntity);
    }
}




