package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.entities.AccountEntity;
import com.desafio2.bancomarcela.models.repositories.AccountsRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/accounts")

public class AccountControllers {

    private final AccountsRepository accountsRepository;

    public AccountControllers(AccountsRepository accountsRepository)
    {
        this.accountsRepository=accountsRepository;
    }

    @GetMapping("/{userid}")
    public List<AccountEntity> getUserAccounts(@PathVariable String userid){
        return accountsRepository.findAllByUserid(userid);
    }

    @PostMapping("/new")
    public AccountEntity newAccount(@RequestBody AccountEntity accountEntity){
        return accountsRepository.save(accountEntity);
    }



}
