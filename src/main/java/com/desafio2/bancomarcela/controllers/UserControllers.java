package com.desafio2.bancomarcela.controllers;

import com.desafio2.bancomarcela.models.entities.UserEntity;
import com.desafio2.bancomarcela.models.repositories.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")

public class UserControllers {
    private final UserRepository userRepository;

    public UserControllers(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @GetMapping("/{dni}")
    public List<UserEntity> getUserDni(@PathVariable String dni) {
        return userRepository.findAllByDni(dni);

    }

    @PostMapping("/new")
    public UserEntity newUser(@RequestBody UserEntity userEntity) {
        return userRepository.save(userEntity);

    }
}
