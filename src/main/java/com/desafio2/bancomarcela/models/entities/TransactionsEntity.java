package com.desafio2.bancomarcela.models.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.math.BigDecimal;

@Entity(name="transactions")

public class TransactionsEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private  Long id;
    private  String date;
    private String description;
    private BigDecimal amount;
    private String currency;
    private String origin;
    private String destination;

    public TransactionsEntity(){}

    public TransactionsEntity(Long id, String date, String description, BigDecimal amount, String currency, String origin, String destination){
        this.id=id;
        this.date=date;
        this.description=description;
        this.amount=amount;
        this.currency=currency;
        this.origin=origin;
        this.destination=destination;

    }

    public Long getId(){
        return id;
    }

    public String getDate(){
        return date;
    }

    public String getDescription(){
        return description;
    }
    public BigDecimal getAmount(){return amount;}
    public String getCurrency(){return currency;}

    public String getOrigin(){return origin;}

    public String getDestination(){return destination;}


}
