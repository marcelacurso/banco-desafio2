package com.desafio2.bancomarcela.models.entities;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name="users")
public class UserEntity {
    @Id
    private String dni;
    private String firtName;
    private String lastName;
    private String adress;

    public UserEntity(){}

    public UserEntity(String dni, String firtName, String lastName, String adress){
        this.dni=dni;
        this.firtName=firtName;
        this.lastName=lastName;
        this.adress=adress;


    }

    public String getDni(){
        return dni;
    }

    public String getFirtName(){
        return firtName;
    }

    public String getLastName(){
        return lastName;
    }
    public String getAdress(){
        return adress;
    }
}
