package com.desafio2.bancomarcela.models.repositories;


import com.desafio2.bancomarcela.models.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

public interface UserRepository extends CrudRepository<UserEntity, Long> {
    List<UserEntity> findAllByDni(String dni);


    @GetMapping("/{dni}")
    default List<UserEntity> getUserDni(@PathVariable String dni){
        return findAllByDni(dni);
    }
}